<?php
include 'header.php';
?>
  <section id="intro-imagine-academy">
    <div class="container">
      <div class="content">
        <h2><?php echo $lang['Microsoft Imagine Academy']; ?></h2>
        <p class="intro-txt"><?php echo $lang['subhead-imagineacademy']; ?></p>
        <div>
        <?php if ( $_SESSION["userdata"]['schoolEmail'] == '' ) { ?>
          <a href="settings" class="intro-submit-button scrollto" style="padding: 15px 75px;"><?php echo $lang['Take Me There']; ?></a>
        <?php } else if ( $_SESSION["userdata"]['isSchoolEmailActivated'] == false ) { ?>
          <a href="settings" class="intro-submit-button scrollto" style="padding: 15px 75px;"><?php echo $lang['Take Me There']; ?></a>
        <?php } else { ?>
          <a href="https://www.microsoft.com/en-us/education/imagine-academy/default.aspx" target="_blank" class="intro-submit-button scrollto" style="padding: 15px 75px;"><?php echo $lang['Take Me There']; ?></a>
        <?php } ?>
        </div>
      </div>
    </div>
  </section><!-- #intro -->

  <main id="main">
  <section id="writeup">
    <div class="container">
      <div class="content imagine-academy-content">
        <h3><?php echo $lang['Choose Over 700 Courses By Microsoft And Learn For FREE!']; ?></h3>
        <p class="normal-txt"><?php echo $lang['writeup-imagineacademy']; ?></p>
        <div class="row">
          <div class="col-lg-3">
            <div class="imagine-academybox">
              <img src="assets/img/imagine-academy-cs.jpg" alt="" />
            </div>
            <h5><?php echo $lang['Computer Science']; ?></h5>
          </div>

          <div class="col-lg-3">
            <div class="imagine-academybox">
              <img src="assets/img/imagine-academy-it.jpg" alt="" />
            </div>
            <h5><?php echo $lang['IT Infrastructure']; ?></h5>
          </div>

          <div class="col-lg-3">
            <div class="imagine-academybox" >
              <img src="assets/img/imagine-academy-ds.jpg" alt="" />
            </div>
            <h5><?php echo $lang['Data Science']; ?></h5>
          </div>

          <div class="col-lg-3" >
            <div class="imagine-academybox">
              <img src="assets/img/imagine-academy-productivity.jpg" style="width:65%" alt="" />
            </div>
            <h5><?php echo $lang['Productivity']; ?></h5>
          </div>
          

        </div>
      </div>
    </div>
  </section><!-- #writeup -->

  <section id="howtoredeem">
    <div class="container">
    <h3><?php echo $lang['How To Redeem']; ?></h3>
      <div class="row">
        <div class="col-lg-4">
          <img src="assets/img/imagine-verify.png" width="180" alt="" />
          <h3><?php echo $lang['Verify University Partner']; ?></h3>
          <p class="normal-txt"><?php echo $lang['verify-partner']; ?></p>
        </div>
        <div class="col-lg-4">
          <img src="assets/img/imagine-confirm.png" width="180" alt="" />
          <h3><?php echo $lang['Email Confirmation']; ?></h3>
          <p class="normal-txt"><?php echo $lang['email-confirm']; ?></p>
        </div>
        <div class="col-lg-4">
          <img src="assets/img/imagine-learning.png" width="180" alt="" />
          <h3><?php echo $lang['Start Learning']; ?></h3>
          <p class="normal-txt"><?php echo $lang['start-learning']; ?></p>
        </div>
      </div>
    </div>
  </section><!-- #howtoredeem -->
  
  <section id="partners">
    <div class="container">
    <h3><?php echo $lang['University Partners']; ?></h3>
    <br clear="both" />
      <div class="row">
          <div class="col-lg-4">
            <div class="imagine-academybox">
              <img src="assets/img/imagine-academy-part-uitm.jpg" alt="" />
            </div>
          </div>

          <div class="col-lg-4">
            <div class="imagine-academybox">
              <img src="assets/img/imagine-academy-part-upsi.jpg" alt="" />
            </div>
          </div>

          <div class="col-lg-4">
            <div class="imagine-academybox">
              <img src="assets/img/imagine-academy-part-upm.jpg" alt="" />
            </div>
          </div>
      </div>

      <div class="row">
          <div class="col-lg-4">
            <div class="imagine-academybox">
              <img src="assets/img/imagine-academy-part-mmu.jpg" alt="" />
            </div>
          </div>

          <div class="col-lg-4">
            <div class="imagine-academybox">
              <img src="assets/img/imagine-academy-part-ukm.jpg" alt="" />
            </div>
          </div>

          <div class="col-lg-4">
            <div class="imagine-academybox">
              <img src="assets/img/imagine-academy-part-usm.jpg" alt="" />
            </div>
          </div>
      </div>

    </div>
  </section><!-- #products -->

  <section id="knowmore">
    <div class="container">
      <h1><?php echo $lang['knowmore']; ?></h1>
      <p><?php echo $lang['knowmore-txt']; ?></p>
      <div>
          <a href="institution-list" class="form-submit-button scrollto" style="padding: 15px 75px;background:#F2BF02;"><?php echo $lang['ASK NOW!']; ?></a>
      </div>
    </div>
  </section><!-- #howtoredeem -->

  </main>

<?php
include 'footer.php';
?>
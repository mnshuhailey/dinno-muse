<?php
include 'header.php';
include 'app/usercontroller.php';
?>
<main id="main">
<!--==========================
      Verify Section
    ============================-->
    <section id="signup" class="wow forgot-pass">
      <div class="container">
        <div class="row">
          <div class="forgot-pass-container">     
            <p><?php echo $lang['verify-txt']; ?></p>
            <form name="verify-school-email" action="" method="POST">
              <?php if ( $_SESSION["errorVerify"] != '' ) { ?>
                      <span id="errMsg" class="error" style="margin-left: 0px;display:block"><?php echo $_SESSION["errorVerify"]; ?></span>
              <?php } ?>
              <?php if ( $_SESSION["successVerify"] != '' ) { ?>
                    <span id="errMsg" class="error" style="margin-left: 0px;color:green !important; font-weight:bold;display:block"><?php echo $_SESSION["successVerify"]; ?></span>
              <?php } ?>
              <input type="email" id="institution-email" class="input email" name="institution-email" placeholder="Institution Email" value="<?php echo $_REQUEST['email']; ?>" required>
              <input type="text" id="verify-code" class="input email" name="verify-code" placeholder="Verification Code" value="<?php echo $_REQUEST['code']; ?>" required>

              <input type="submit" class="form-submit-button forgot-pass-button" name="verify-account" value="<?php echo $lang['verifyaccount']; ?>"/>
              
            </form>  
          </div> 
        </div>
      </div>
    </section>
</main>

<?php
include 'footer.php';
?>

<?php
include 'header.php';
include 'app/pagescontroller.php';
?>
  
  <main id="main">
    <section id="newslist">
      <div class="container">
        <div class="row">
          <div class="col-lg-6">
            <?php 
            $dataNews = getNewsLatest(); 
            while ( $dataNewsLatest = getObject($dataNews) ) {
              if ( $_SESSION["lang"] == 'bm' ) {
                $news_title = $dataNewsLatest->news_title_bm;
                $news_desc  = $dataNewsLatest->news_description_bm;
              } else {
                $news_title = $dataNewsLatest->news_title_en;
                $news_desc  = $dataNewsLatest->news_description_en;
              }
              
            ?>
              <div class="newsmainbox" style="margin-bottom:10px;">
                <a href="news-details?nid=<?php echo $dataNewsLatest->news_id; ?>"><img class="news-img1" src="app/timthumb.php?src=<?php echo UPLOADS_URL; ?>news/<?php echo $dataNewsLatest->news_image; ?>&w=490&h=515" alt="" />
                <div class="newsmainbox-info">
                  <div class="news-title"><h3><?php echo $news_title; ?></h3></div>
                  <div class="news-desc"><?php echo substr($news_desc, 0, 190).' ...'; ?></div>
                </div>
                </a>
              </div>
            <?php  
            }
            ?>
          </div>
          <div class="col-lg-6">
            <?php 
            $i = 1;
            $dataNews = getNews();
            $ignoreID = getObject($dataNews); 
            while ( $dataNewsLatest = getObject($dataNews) ) {
              if ( $ignoreID->news_id == $dataNewsLatest->news_id ) continue;
              if ( $_SESSION["lang"] == 'bm' ) {
                $news_title = $dataNewsLatest->news_title_bm;
                $news_desc  = $dataNewsLatest->news_description_bm;
              } else {
                $news_title = $dataNewsLatest->news_title_en;
                $news_desc  = $dataNewsLatest->news_description_en;
              }
              
            ?>
              <div class="newsmainbox" style="margin-bottom:10px;">
                <a href="news-details?nid=<?php echo $dataNewsLatest->news_id; ?>"><img class="news-img2" src="app/timthumb.php?src=<?php echo UPLOADS_URL; ?>news/<?php echo $dataNewsLatest->news_image; ?>&w=490&h=252" alt="" />
                <div class="newsmainbox-info<?php echo $i; ?>">
                  <div class="news-title"><h3><?php echo $news_title; ?></h3></div>
                  <div class="news-desc"><?php echo substr($news_desc, 0, 130).' ...'; ?></div>
                </div>
                </a>
              </div>
            <?php 
            $i++; 
            }
            ?>
          </div>
        </div>
        <div class="btn-more-events">
          <a href="all-news"><?php echo $lang['btn-news-more']; ?> <img src="assets/img/more-btn.png" alt=""></a>
        </div>
      </div>
    </section>

    <section id="eventlist">
      <div class="container">
      <h3><?php echo $lang['eventhappening']; ?></h3>
        <div class="row">
        <?php
        $m = 1; 
        $dataEvents = getEventsLatest(); 
        while ( $dataEventsLatest = getObject($dataEvents) ) {
          if ( $_SESSION["lang"] == 'bm' ) {
            $events_title = $dataEventsLatest->events_title_bm;
            $events_desc  = $dataEventsLatest->events_description_bm;
          } else {
            $events_title = $dataEventsLatest->events_title_en;
            $events_desc  = $dataEventsLatest->events_description_en;
          }
        ?>
          <div class="col-lg-4">
            <div class="eventsmainbox">
              <a href="all-events"><img class="event-img" src="app/timthumb.php?src=<?php echo UPLOADS_URL; ?>events/<?php echo $dataEventsLatest->events_image; ?>&w=300&h=157" width="300" height="157" alt="" />
              <div class="eventsmainbox-info" style="min-height:123px;">
                <div class="news-title"><h5><?php 
                  if(strlen($events_title) > 40 ) { $dot = '...'; } else { $dot = ''; }
                  echo substr($events_title, 0, 40).$dot; 
                ?></h5></div>
                <div class="news-desc"><?php 
                  if(strlen($events_desc) > 80 ) { $dotd = '...'; } else { $dotd = ''; }
                  echo substr($events_desc, 0, 80).' '.$dotd; 
                ?></div>
              </div>
              </a>
            </div>
          </div>
        <?php  
        $m++; 
          }
        ?>
        </div>
        <div class="btn-more-events">
          <a href="all-events"><?php echo $lang['btn-events-more']; ?> <img src="assets/img/more-btn.png" alt=""></a>
        </div>
      </div>
    </section>
    

  </main>

<?php
include 'footer.php';
?>
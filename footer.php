<!--==========================
  Footer
============================-->
  <footer id="footer">
    <div class="sub-footer">
      <div class="container">
      <div class="row">
        <div class="col-lg-4 col-md-4">
          <h6><?php echo $lang['aboutmuse']; ?></h6>
          <p><?php echo $lang['museinfo']; ?></p>
        </div>
        <div class="col-lg-4 col-md-4 spacing">
          <h6><?php echo $lang['link']; ?></h6>
          <ul class="link">
            <li><a href="office365"><?php echo $lang['office365']; ?></a></li>
            <li><a href="imagine"><?php echo $lang['Imagine Software']; ?></a></li>
            <li><a href="imagine-academy"><?php echo $lang['imagineacademy']; ?></a></li>
            <!--<li><a href="windows10"><?php echo $lang['Windows 10']; ?></a></li>-->
            <li><a href="news-events"><?php echo $lang['News & Events']; ?></a></li>
          </ul>
        </div>
        <div class="col-lg-4 col-md-4">
          <h6><?php echo $lang['collaboration']; ?></h6>
          <div class="collaborate1"><img src="assets/img/KPM Logo.png" width="130" alt=""></div>
          <div class="collaborate2"><img src="assets/img/prestariang-logo.png" width="100" alt=""></div>
        </div>
      </div>
      </div>
    </div>
    <div class="container">
      <div class="copyright">
        Copyright 2018 &copy; Prestariang Sdn Bhd. All Rights Reserved.
      </div>
    </div>
  </footer><!-- #footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

  <!-- JavaScript Libraries -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script src="lib/jquery/jquery-migrate.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="lib/easing/easing.min.js"></script>
  <script src="lib/superfish/hoverIntent.js"></script>
  <script src="lib/superfish/superfish.min.js"></script>
  <script src="lib/wow/wow.min.js"></script>
  <script src="lib/owlcarousel/owl.carousel.min.js"></script>
  <script src="lib/magnific-popup/magnific-popup.min.js"></script>
  <script src="lib/sticky/sticky.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
  <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>

  <!-- Template Main Javascript File -->
  <script src="assets/js/main.js?v=<?php echo time(); ?>"></script>
  <?php
  if ( $_SESSION['successregister'] === TRUE ) {
  ?>
  <script>
      jQuery(document).ready(function( $ ) {
        $("#signup-respond").modal({
          escapeClose: false,
          clickClose: false,
          showClose: false,
          fadeDuration: 100
        });
      });
  </script>
  <?php
  }
  ?>
<?php if ( $_SESSION["successAcc"] != '' ) { ?>
<script>
  jQuery(document).ready(function( $ ) {
    $("#complete-schoolemail").modal({
      escapeClose: false,
      clickClose: false,
      showClose: false
    });
  });
</script>
<?php } ?> 
</body>
</html>

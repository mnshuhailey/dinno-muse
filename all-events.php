<?php
include 'header.php';
include 'app/pagescontroller.php';
$currentYear = date("Y");
$currentMonth = date("M");
$months = array(
  '1' => 'Jan',
  '2' => 'Feb',
  '3' => 'Mar',
  '4' => 'Apr',
  '5' => 'May',
  '6' => 'Jun',
  '7' => 'Jul',
  '8' => 'Aug',
  '9' => 'Sep',
  '10' => 'Oct',
  '11' => 'Nov',
  '12' => 'Dec');
?>

  <main id="main">
    <section id="allevents">
        <div class="allevents-filter">
          <div class="container">
          <form name="filter" action="" method="post">
            <label class="month"><?php echo $lang['when'];?></label>
            <select id="month" class="month" name="month" >
              <option value=""><?php echo $lang['allmonth'];?></option>
              <?php foreach ( $months as $key => $month ) { ?>
                <option value="<?php echo $key; ?>" <?php if ($_REQUEST['m'] == $key ) echo 'selected';?> ><?php echo $month; ?></option>
              <?php } ?>
            </select>
            <select id="year" class="month" name="year" >
              <?php for ($year = 2018; $year < 2023; $year++ ) { ?>
                <option value="<?php echo $year; ?>" <?php if ($_REQUEST['y'] == $year) { echo 'selected'; } elseif ($currentYear == $year) { echo 'selected'; }?> ><?php echo $year; ?></option>
              <?php } ?>
            </select>
            </form>
          </div>
        </div>
        <div class="container">
        <h3><?php echo $lang['allevent'];?></h3>
            <?php
              if ( isset($_REQUEST['m']) AND isset($_REQUEST['y']) ) {
               $dataEvents = getAllEventsFilter($_REQUEST['m'], $_REQUEST['y']);
              } else {
                $dataEvents = getAllEvents();
              }

              $rowCount = mysqli_num_rows ($dataEvents);
              if ( $rowCount > 0 ) {

            while ( $dataAllEvents = getObject($dataEvents) ) {
              if ( $_SESSION["lang"] == 'bm' ) {
                $events_title = $dataAllEvents->events_title_bm;
                $events_desc  = $dataAllEvents->events_description_bm;
                $fee          = $dataAllEvents->fee;
                if ( $fee == 'FREE' ) {
                  $fee = 'TIADA';
                } else {
                  $fee = $dataAllEvents->fee;
                }
              } else {
                $events_title = $dataAllEvents->events_title_en;
                $events_desc  = $dataAllEvents->events_description_en;
                if ( $fee == 'FREE' ) {
                  $fee = 'FREE';
                } else {
                  $fee = $dataAllEvents->fee;
                }
              }

            ?>
              <div class="row alleventsbox">
                <div class="col-lg-3"><img class="allevent-img" src="app/timthumb.php?src=<?php echo UPLOADS_URL; ?>events/<?php echo $dataAllEvents->events_image;?>&w=160&h=160" alt="" /></div>
                <div class="col-lg-7">
                  <div class="alleventsbox-info">
                    <div class="events-title"><h5><?php echo $events_title; ?></h5></div>
                    <div class="events-desc"><?php echo $events_desc; ?></div>
                    <div class="events-datetime-info">
                      <div class="events-time"><?php echo $lang['time']; ?>: <?php echo date("h:i A",strtotime($dataAllEvents->time_start)); ?></div>
                      <div class="events-date"><?php echo $lang['dateStart']; ?>: <?php echo date("F d Y",strtotime($dataAllEvents->date_start)); ?></div>
                      <div class="events-date"><?php echo $lang['dateEnd']; ?>: <?php echo date("F d Y",strtotime($dataAllEvents->date_end)); ?></div>
                      <div class="events-fee"><?php echo $lang['fee']; ?>: <?php echo $fee; ?></div>
                    </div>
                  </div>
                </div>
                <div class="col-lg-2">
                  <div class="eventdate-box">
                    <div class="day">
                    <?php
                    $days = date("N",strtotime($dataAllEvents->date_start));
                    foreach ( LIST_DAYS as $day => $val ) {
                      if ( $day == $days ) {
                        echo $val;
                      }
                    }
                    ?></div>
                    <div class="month-date">
                      <div class="month-date-m"><?php echo date("F",strtotime($dataAllEvents->date_start)); ?></div>
                      <div class="month-date-d"><?php echo date("d",strtotime($dataAllEvents->date_start)); ?></div>
                    </div>
                  </div>
                </div>
              </div>
            <?php
            }

          }else{ ?>
            <div class="">
              <p><?php echo $lang['nodataevent'];?></p>
            </div>
          <?php } ?>
      </div>
    </section>

  </main>

<?php
include 'footer.php';
?>

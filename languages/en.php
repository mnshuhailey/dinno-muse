<?php

$lang['dateStart'] = "START";
$lang['dateEnd'] = "ENDS";

$lang['nodataevent'] = 'No events held on the selected month and year';

$lang['verify-email'] = 'Please update and verify your Institution Email address.';
$lang['verify-email-msg'] = 'Please verify your Institution Email address.';
$lang['send-email-verify'] = 'A verification link has been sent to your institution email account. Please check your email to verify your account. Thank you.';
$lang['verify-email-header'] = 'Verify Institution Email';
$lang['verify-txt'] = 'Please enter your Institution Email and Verification Code';
$lang['verifyaccount'] = 'Verify';
$lang['successverify'] = 'Congratulations, your Institution Email has been verified.';
$lang['successverifypending'] = 'Congratulations, your verification info has been submitted.';

$lang['allmonth'] = 'All Month';
$lang['allyear'] = 'All Year';

//Page Title
$lang['imagine-academy'] = 'Imagine Academy';
$lang['windows10'] = 'Windows 10';
$lang['news-events'] = 'News & Event';
$lang['verify-account'] = 'Verify Account';
$lang['forgot-password'] = 'Forgot Password';
$lang['adobe'] = 'Adobe';
$lang['autodesk'] = 'Autodesk';

// Navigation
$lang['home'] = 'HOME';
$lang['software'] = 'SOFTWARE';
$lang['office365'] = 'Office 365';
$lang['imagine'] = 'Imagine';
$lang['freecourses'] = 'FREE COURSES';
$lang['imagineacademy'] = 'Imagine Academy';
$lang['windows'] = 'WINDOWS 10';
$lang['news'] = 'NEWS & EVENTS';
$lang['freesignup'] = 'FREE SIGN UP';

// User Login
$lang['settings'] = 'Settings';
$lang['logout'] = 'Log Out';

// Footer
$lang['aboutmuse'] = 'ABOUT MUSE';
$lang['museinfo'] = 'The easiest way to discover benefits from top industry partners catered specially for the student community in Malaysia.';
$lang['link'] = 'LINK';
$lang['Imagine Software'] = 'Imagine Software';
$lang['Windows 10'] = 'Windows 10';
$lang['News & Events'] = 'News & Events';
$lang['collaboration'] = 'MUSE IS A COLLABORATION BETWEEN:';

// Login Form
$lang['signup'] = 'SIGN UP';
$lang['login'] = 'LOG IN';
$lang['email'] = 'Email Address*';
$lang['password'] = 'Password*';
$lang['First time sign in'] = 'First time sign in';
$lang['Sign Up'] = 'Sign Up';
$lang['Forgot Password'] = 'Forgot Password';

// Signup Form
$lang['signup-header-txt'] = 'Sign Up and Get Your Student Benefits';
$lang['fullname'] = 'Full Name';
$lang['lastname'] = 'Last Name';
$lang['signup-agree-txt'] = 'By signup up, you agree to the MUSE';
$lang['Term of Services'] = 'Term of Services';
$lang['tncapply'] = 'Term and Condition Apply';

//Singup Extra
$lang['Complete These Fields'] = 'Complete These Fields';
$lang['nric'] = 'IC Number';
$lang['phone'] = 'Phone Number*';
$lang['continue'] = 'Continue';

//Verify Account
$lang['Verify your account'] = 'Verify your account';
$lang['inst-name'] = 'Institution Name';
$lang['inst-email'] = 'Institution Email*';
$lang['sendemail'] = 'SEND EMAIL';
$lang['welcomeverify'] = 'Welcome Onboard';
$lang['verifymsg'] = 'Thanks you for signing up with MUSE. A verification link has been sent to your email account.';

//Forgot Password
$lang['forgotpassheader-txt'] = 'Forgot Your Password?';
$lang['forgotpasssubheader-txt'] = 'Enter your email address below and we will help you';
$lang['resetpass'] = 'RESET MY PASSWORD';
$lang['Take me to'] = 'Take me to';
$lang['Sing In'] = 'Sign In';

//Settings
$lang['annouce-txt'] = 'Complete your profile to enjoy exclusive student benefits!';
$lang['Personal Information'] = 'Personal Information';
$lang['Account Information'] = 'Account Information';
$lang['Credentials'] = 'Credentials';
$lang['dob'] = 'Date Of Birth';
$lang['ic-passport'] = 'IC/Passport';
$lang['gender'] = 'Gender';
$lang['current-pass'] = 'Current Password';
$lang['new-pass'] = 'New Password';
$lang['verify-pass'] = 'Verify Password';
$lang['save'] = 'SAVE';
$lang['reg-email'] = 'Registered Email';
$lang['inst-email'] = 'Institution Email';

// Imagine
$lang['Microsoft Imagine'] = 'Microsoft Imagine';
$lang['subhead-imagine'] = 'Sparking your dreams, with great learning tools';
$lang['Take Me There'] = 'TAKE ME THERE!';
$lang['writeup-head'] = '20 Downloadable Software To Be Grabbed From Microsoft For FREE';
$lang['writeup-content'] = 'Microsoft Imagine offers FREE Microsoft products for student with MUSE to build apps, video games and also other projects.<br> All you have to do is redeem your access and enjoy the world of possibilities ahead.';
$lang['How To Redeem'] = 'How To Redeem';
$lang['Verify University Partner'] = 'Verify University Partner';
$lang['verify-partner'] = 'Verify that you are student of one MUSE University partners';
$lang['Email Confirmation'] = 'Email Confirmation';
$lang['email-confirm'] = 'Contact your university and get the confirmation on your email';
$lang['Start Learning'] = 'Start Learning';
$lang['start-learning'] = 'Sign up the offers using the verified university email and start learning for free!';
$lang['product-title'] = 'Products You Do Not Want To Miss';
$lang['Operating Systems'] = 'Operating Systems';
$lang['Product include:'] = 'Product include:';
$lang['Developer & Design Tools'] = 'Developer & Design Tools';
$lang['Applications'] = 'Applications';
$lang['Servers'] = 'Servers';
$lang['knowmore'] = 'WANT TO KNOW MORE?';
$lang['knowmore-txt'] = 'Contact your institution here.';
$lang['ASK NOW!'] = 'ASK NOW!';

// Office365
$lang['Microsoft Office 365'] = 'Microsoft Office 365';
$lang['subhead-office365'] = 'Empowering your education productivity';
$lang['Microsoft Office 365 for Education'] = 'Microsoft Office 365 for Education';
$lang['office365-edu'] = 'Microsoft Office 365 for Education is a collection  of services that allows you to collaborate and share your schoolwork. It is available for free to teachers who are currently working at an academic institution and to students who are  currently enrolling in an academic institution.';
$lang['CLAIM YOURS!'] = 'CLAIM YOURS!';
$lang['Microsoft Office 365 ProPlus'] = 'Microsoft Office 365 ProPlus';
$lang['office365-proplus'] = "Microsoft Office 365 ProPlus includes productivity software where you can just install in your laptop. It always have the latest version of your familiar Office applications, no matter which device you're using-PC/Mac, tablet or phone.";

// Imagine Academy
$lang['Microsoft Imagine Academy'] = 'Microsoft Imagine Academy';
$lang['subhead-imagineacademy'] = 'Get trained and certified by Microsoft for FREE today!';
$lang['Choose Over 700 Courses By Microsoft And Learn For FREE!'] = 'Choose Over 700 Courses By Microsoft And Learn For FREE!';
$lang['writeup-imagineacademy'] = 'MUSE provide a platform to train and certify students with up-to-date curriculum and resources on Microsoft products and technologies through Microsoft Imagine Academy. Student acquire worthy skills for college and career that will excel them in the increasingly digital global economy areas:';
$lang['Computer Science'] = 'Computer Science';
$lang['IT Infrastructure'] = 'IT Infrastructure';
$lang['Data Science'] = "Data Science";
$lang['Productivity'] = "Productivity";
$lang['University Partners'] = "University Partners";

//Windows 10
$lang['Windows 10'] = 'Windows 10 Education';
$lang['subhead-windows10'] = 'Get the best version of Windows on your device';
$lang['Experience The Wonders of Windows 10 Education'] = 'Experience The Wonders of Windows 10 Education';
$lang['CLAIM YOUR ACCESS'] = 'CLAIM YOUR ACCESS';
$lang['windows10-writeup'] = "Windows 10 Education is Microsoft's most robust version for Windows operating system. It is designed for students and also is workplace ready. Enjoy the improved start menu, the new Edge browser, enhanced security and more. <br> All you have to do is confirm your university partner and claim your access from MUSE. Enjoy the whole new experinece of the latest Windows for Education.";
$lang['Familiar Features'] = 'Familiar Features';
$lang['Verified for Security'] = 'Verified for Security';
$lang['Great Perfomance'] = 'Great Performance';
$lang['Easy Setup'] = 'Easy Setup';
$lang['familiar-features-writeup'] = 'Experience the newest innovations, and also available fully-installed Office applications on any device.';
$lang['verified-security-writeup'] = 'Security at its simplest. Applications are selected and delivered from the Microsoft Store for Education.';
$lang['great-perfomance-writeup'] = 'Designed to deliver performance that keeps up in the classroom. Start-ups are quick and built to stay that way.';
$lang['easy-setup-writeup'] = 'Cloud-connected and easily managed side-by-side with other editions of Windows 10, using the free Set Up School PCs app.';

//Verify Profile
$lang['titlecompleteprofile'] = 'Please Complete Your Profile To Proceed.';
$lang['completeprofilemsg'] = 'Before you can enjoy these student perks in MUSE, you will need to complete your information.';

// Home
$lang['home-title'] = 'Exclusive benefits for students';
$lang['subhead-home'] = 'Get instant access to over RM 50,000 worth of software, training and study resources for free!';
$lang['Join Now!'] = 'JOIN NOW!';
$lang['ourpartner'] = 'OUR<br>PARTNER';
$lang['getwithmusetitle'] = 'What can you get with MUSE';
$lang['Free Software'] = 'FREE SOFTWARE';
$lang['freesoftware-txt'] = 'Redeem software worth more than RM 50,000 for FREE';
$lang['Learn for free'] = 'LEARN FOR FREE';
$lang['learnfree-txt'] = 'Get access to unlimited, premium online learning content at NO COST';
$lang['Explore Career'] = 'EXPLORE CAREER';
$lang['career-txt'] = 'Discover career path with your new equipped skills';

$lang['column1-title'] = 'Obtain the best-in-class learning tools';
$lang['column1-content'] = 'We bring you the best learning tools and software to boost your academic productivity. From productivity to design software, we bring you all the tools you need to support your learning experience';
$lang['column1-content2'] = 'Worth more than RM 50,000 - free for all MUSE members';
$lang['column2-title'] = 'Prepare for Careers of the Future';
$lang['column2-content'] = 'Get free training and certification to equip yourself with skills of tomorrow. Through our exclusive partnership with Microsoft Imagine Academy, you can gain valuable skills to succeed in the digital economy';
$lang['column2-content2'] = '758 courses available - free for all MUSE members';
$lang['column3-title'] = 'Explore Career Path';
$lang['column3-content'] = 'Not only that we train and provide you with the best tools to prepare you for career, we help you explore suitable career path. Discover careers of tomorrow and find out what it takes to land your dream job.';
$lang['column3-content2'] = 'More than 5000 jobs available - exclusive for MUSE members';

$lang['howtostart'] = 'How to start enjoying all your benefits now';
$lang['registerwithmuse'] = 'Register with MUSE';
$lang['verifystudentstatus'] = 'Verify your student status';
$lang['studentbenefit'] = 'Enjoy student benefits';

$lang['homeslider-title'] = 'Anticipate your career with us today';
$lang['homeslider-title1'] = 'Azure Cloud Engineer';
$lang['homeslider-center1'] = 'Microsoft Malaysia<br>KLCC, Kuala Lumpur<br>Expected Salary: RM 3,000';
$lang['homeslider-title2'] = '3D Development Analyst';
$lang['homeslider-center2'] = 'Autodesk Malaysia<br>Axiata Tower, Kuala Lumpur<br>Expected Salary: RM 5,000';
$lang['homeslider-title3'] = 'Sharepoint Engineer';
$lang['homeslider-center3'] = 'Microsoft Malaysia<br>KLCC, Kuala Lumpur<br>Expected Salary: RM 4,000';

$lang['allbenefit-title'] = 'ALL YOUR BENEFITS, WITH ONE ACCESS';
$lang['GET STARTED!'] = 'GET STARTED!';

$lang['CLICK HERE'] = 'CLICK HERE';
$lang['attention-title'] = 'ATTENTION TO INTERNATIONAL STUDENTS!';
$lang['attention-list1'] = 'Medical Insurance Protection?';
$lang['attention-list2'] = 'Student Welfare and Engagement?';
$lang['attention-list3'] = 'VISA/iKad renewal?';
$lang['attention-list4'] = 'Customer Service?';

// Events
$lang['eventhappening'] = 'Events Happening';
$lang['newsevents'] = 'News and Events';
$lang['allevent'] = 'Check Out These Cool Events!';
$lang['btn-news-more'] = 'More News';
$lang['btn-events-more'] = 'More Events';
$lang['time'] = 'TIME';
$lang['date'] = 'DATE';
$lang['fee'] = 'FEE';
$lang['category'] = 'Category';
$lang['when'] = 'When';
$lang['All Events Happening'] = 'All Events Happening';
$lang['This Month'] = 'This Month';
$lang['allnews'] = 'All News';

// Days
$lang['Monday'] = 'MONDAY';
$lang['Tuesday'] = 'TUESDAY';
$lang['Wednesday'] = 'WEDNESDAY';
$lang['Thursday'] = 'THURSDAY';
$lang['Friday'] = 'FRIDAY';
$lang['Saturday'] = 'SATURDAY';
$lang['Sunday'] = 'SUNDAY';

// Adobe
$lang['Adobe'] = 'Adobe';
$lang['subhead-adobe'] = 'Coming Soon!';
$lang['adobe-title1'] = 'WEB TOOLS';
$lang['adobe-content1'] = 'Turn your brightest ideas into exceptional experiences with our family of web and UX design tools. Build modern responsive sites with or without coding. And choose from thousands of fonts to make your sites look just the way you want.';
$lang['adobe-title2'] = 'DESIGN TOOLS';
$lang['adobe-content2'] = 'The world-class design tools in Creative Cloud give you everything you need to make anything you can dream up. Combine images to make incredible artwork. And use our mobile apps to sketch, draw, and create layouts wherever you’re inspired.';
$lang['adobe-title3'] = 'VIDEO AND AUDIO TOOLS';
$lang['adobe-content3'] = 'From home movies and YouTube videos to Hollywood blockbusters, Creative Cloud has you covered. Add graphics, effects, and pro-quality audio. Your desktop and mobile apps work together seamlessly, so you can take your footage further on film, TV, and the web.';

// Autodesk
$lang['Autodesk'] = 'Autodesk';
$lang['subhead-autodesk'] = 'Coming Soon!';

//Form Validation
$lang['valid-email'] = 'Email Address required!';
$lang['valid-pass'] = 'Password required!';
$lang['valid-email-pattern'] = 'Email Address not valid!';
$lang['emailpass-wrong'] = 'Email Address / Password not valid!';
$lang['valid-ic'] = 'IC Number required!';
$lang['valid-phone'] = 'Phone Number required!';
$lang['inst-email-required'] = 'Institution Email required!';
$lang['inst-name-required'] = 'Institution Name required!';
$lang['warning-msg-form'] = 'Please fill in the fields below!';
$lang['success-msg-form'] = 'Successfully Updated!';
$lang['curpass-wrong-form'] = 'Current password incorrect!';
$lang['newpass-wrong-form'] = 'New password not match!';
$lang['newpass-less-form'] = 'Passwords must be at least 8 characters and contain the following: upper case (A-Z), lower case (a-z), number (0-9) and special character (e.g. !@#$%^&*)';
$lang['valid-fullname'] = 'Full Name required!';

$lang['sortby'] = 'Sort by';
$lang['nameofinst'] = 'Name of Institution';
$lang['location'] = 'Location';
$lang['Find Your Institution Here'] = 'Find Your Institution Here';
$lang['emaillist'] = 'Email';
$lang['contactno'] = 'Contact No.';

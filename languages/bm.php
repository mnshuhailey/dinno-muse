<?php

$lang['dateStart'] = "MULA";
$lang['dateEnd'] = "TAMAT";

$lang['nodataevent'] = 'Tiada acara yang diadakan pada bulan dan tahun yang dipilih';

$lang['verify-email'] = 'Sila kemas kini dan sahkan alamat Emel Institusi anda.';
$lang['send-email-verify'] = 'Pautan verifikasi telah dihantar ke akaun e-mel institusi anda. Sila semak e-mel anda untuk mengesahkan akaun anda. Terima kasih.';
$lang['verify-email-header'] = 'Sahkan Emel Institusi';
$lang['verify-txt'] = 'Sila masukkan Emel Institusi dan Kod Pengesahan anda';
$lang['verifyaccount'] = 'Sahkan';
$lang['successverify'] = 'Tahniah, Emel Institusi anda telah disahkan.';
$lang['successverifypending'] = 'Tahniah, maklumat pengesahan anda telah dihantar.';

$lang['allmonth'] = 'Semua Bulan';
$lang['allyear'] = 'Semua Tahun';

//Page Title
$lang['imagine-academy'] = 'Imagine Academy';
$lang['windows10'] = 'Windows 10';
$lang['news-events'] = 'Berita & Acara';
$lang['verify-account'] = 'Pengesahan Akaun';
$lang['forgot-password'] = 'Lupa Kata Laluan';
$lang['adobe'] = 'Adobe';
$lang['autodesk'] = 'Autodesk';

// Navigation
$lang['home'] = 'LAMAN UTAMA';
$lang['software'] = 'PERISIAN';
$lang['office365'] = 'Office 365';
$lang['imagine'] = 'Imagine';
$lang['freecourses'] = 'KURSUS PERCUMA';
$lang['imagineacademy'] = 'Imagine Academy';
$lang['windows'] = 'WINDOWS 10';
$lang['news'] = 'BERITA & ACARA';
$lang['freesignup'] = 'DAFTAR PERCUMA';

// User Login
$lang['settings'] = 'Tetapan';
$lang['logout'] = 'Log Keluar';

// Footer
$lang['aboutmuse'] = 'INFO MUSE';
$lang['museinfo'] = 'Managing University Software as an Enterprise (MUSE) merupakan kontrak berpusat bagi pembekalan produk Microsoft untuk Kementerian Pendidikan Tinggi dan Institusi dibawahnya iaitu Universiti, Kolej Komuniti dan Politeknik.';
$lang['link'] = 'PAUTAN';
$lang['Imagine Software'] = 'Imagine Software';
$lang['Windows 10'] = 'Windows 10';
$lang['News & Events'] = 'Berita & Acara';
$lang['collaboration'] = 'MUSE IALAH KERJASAMA ANTARA';

// Login Form
$lang['signup'] = 'DAFTAR MASUK';
$lang['login'] = 'LOG MASUK';
$lang['email'] = 'Emel*';
$lang['password'] = 'Kata Laluan*';
$lang['First time sign in'] = 'Kali pertama ke sini';
$lang['Sign Up'] = 'Daftar Masuk';
$lang['Forgot Password'] = 'Terlupa Kata Laluan';

// Signup Form
$lang['signup-header-txt'] = 'Daftar dan Nikmati Tawaran Hebat!';
$lang['fullname'] = 'Nama Lengkap';
$lang['lastname'] = 'Nama Keluarga';
$lang['signup-agree-txt'] = 'Dengan mendaftar masuk, anda bersetuju dengan';
$lang['Term of Services'] = 'Terma Servis MUSE';
$lang['tncapply'] = 'Tertakluk Kepada Terma dan Syarat';

//Singup Extra
$lang['Complete These Fields'] = 'Isi Butiran Anda';
$lang['nric'] = 'NRIC';
$lang['phone'] = 'Nombor Telefon*';
$lang['continue'] = 'Teruskan';

//Verify Account
$lang['Verify your account'] = 'Sahkan Akaun Anda';
$lang['inst-name'] = 'Nama Institusi';
$lang['inst-email'] = 'Emel Institusi*';
$lang['sendemail'] = 'HANTAR EMEL';
$lang['welcomeverify'] = 'Selamat Datang';
$lang['verifymsg'] = 'Terima kasih kerana berdaftar bersama MUSE. Pautan verifikasi telah dihantar ke akaun e-mel anda';

//Forgot Password
$lang['forgotpassheader-txt'] = 'Terlupa Kata Laluan?';
$lang['forgotpasssubheader-txt'] = 'Masukkan emel anda di  bawah, dan kami sedia membantu anda';
$lang['resetpass'] = 'SET SEMULA KATA LALUAN';
$lang['Take me to'] = 'Bawa saya intuk';
$lang['Sing In'] = 'Log Masuk';

//Settings
$lang['annouce-txt'] = 'Lengkapkan profil anda untuk menikmati faedah-faedah pelajar yang hebat!';
$lang['Personal Information'] = 'Butiran Peribadi';
$lang['Account Information'] = 'Butiran Akaun';
$lang['Credentials'] = 'Penetapan Keselamatan';
$lang['dob'] = 'Tarikh Lahir';
$lang['ic-passport'] = 'IC/Passport';
$lang['gender'] = 'Jantina';
$lang['current-pass'] = 'Kata Laluan Terkini';
$lang['new-pass'] = 'Kata Laluan Baru';
$lang['verify-pass'] = 'Sahkan Kata Laluan';
$lang['save'] = 'SIMPAN';
$lang['reg-email'] = 'Emel Pendaftaran';
$lang['inst-email'] = 'Emel Institusi';

// Imagine
$lang['Microsoft Imagine'] = 'Microsoft Imagine';
$lang['subhead-imagine'] = 'Tunaikan impian anda bersama peralatan pembelajaran yang gempak';
$lang['Take Me There'] = 'MULAKAN SEKARANG!';
$lang['writeup-head'] = '20 Perisian PERCUMA Microsoft Imagine daripada Microsoft Untuk Didapatkan!';
$lang['writeup-content'] = 'Microsoft Imagine menwarkan servis dan perisian produk untuk pelajar melalui MUSE secara Percuma untuk membina aplikasi mudah alih, perisian permainan, video, dan lain lain. Apa yang perlu anda lakukan, hanya mulakan akses anda dan nukmati dunia inovasi tanpa batasan.';
$lang['How To Redeem'] = 'Langkah-langkah Penebusan';
$lang['Verify University Partner'] = 'Sahkan Kerjasama Universiti Anda';
$lang['verify-partner'] = 'Sahkan bahawa anda merupakan pelajar institusi kerjasama MUSE';
$lang['Email Confirmation'] = 'Sahkan Emel Pelajar Anda';
$lang['email-confirm'] = 'Hubungi institusi anda untuk pengesahan emel pelajar';
$lang['Start Learning'] = 'Mulakan Pembelajaran Anda';
$lang['start-learning'] = 'Daftar masuk dengan menggunakn emel pelajar dan mulakan pembelajaran';
$lang['product-title'] = 'Produk-produk Yang Tidak Mahu Anda Lepaskan!';
$lang['Operating Systems'] = 'Sistem Operasi';
$lang['Product include:'] = 'Produk-produk Termasuk:';
$lang['Developer & Design Tools'] = 'Alatan Pemaju dan Reka Bentuk Web';
$lang['Applications'] = 'Aplikasi';
$lang['Servers'] = 'Pelayan';
$lang['knowmore'] = 'MAHUKAN MAKLUMAT LANJUT?';
$lang['knowmore-txt'] = 'Hubungi institusi anda di sini.';
$lang['ASK NOW!'] = 'KLIK DI SINI!';

// Office365
$lang['Microsoft Office 365'] = 'Microsoft Office 365';
$lang['subhead-office365'] = 'Tingkatkan produktiviti pembelajaran anda';
$lang['Microsoft Office 365 for Education'] = 'Microsoft Office 365 for Education';
$lang['office365-edu'] = 'Microsoft Office 365 Education merupakan koleksi 24 servis yang membenarkan pelajar untuk bekerjasama dan berkongsi tugasan. Servis ini adalah percuma untuk para pelajar dan juga pensyarah di semua institusi yang berkelayakan. Antara servis yang disediakan adalah OneDrive, Skype dan Teams.';
$lang['CLAIM YOURS!'] = 'DAPATKAN!';
$lang['Microsoft Office 365 ProPlus'] = 'Microsoft Office 365 ProPlus';
$lang['office365-proplus'] = "Office 365 ProPlus merangkumi perisian produktiviti dimana anda boleh memasang perisian di dalam komputer, Mac, tablet dan telefon pintar. Antara perisian yang diberikan ialah Microsoft Excel, OneNote dan Publisher.";

// Imagine Academy
$lang['Microsoft Imagine Academy'] = 'Microsoft Imagine Academy';
$lang['subhead-imagineacademy'] = 'Dapatkan latihan serta pengiktirafan daripada Microsoft secara percuma!';
$lang['Choose Over 700 Courses By Microsoft And Learn For FREE!'] = 'Pilih Daripada Lebih 700 Kursus Daripada Microsoft Dan Mula Latihan Secara Percuma';
$lang['writeup-imagineacademy'] = 'MUSE menyediakan platform untuk melatih dan mengiktiraf pelajar dengan ko-kurikulum terkini dan sumber-sumber berpandukan sumber daripda produk Microsoft dan teknologi menerusi Microsoft Imagine Academy. Pelajar memperoleh kemahiran yang bermanfaat untuk institusi dan kerjaya yang akan memacu mereka ke dalam bidang ekonomi digital.';
$lang['Computer Science'] = 'Sains Komputer';
$lang['IT Infrastructure'] = 'Infrastruktur IT';
$lang['Data Science'] = "Sains Data";
$lang['Productivity'] = "Produktiviti";
$lang['University Partners'] = "Kerjasama Universiti";

//Windows 10
$lang['Windows 10'] = 'Windows 10 Education';
$lang['subhead-windows10'] = 'Dapatkan versi Windows yang terbaik untuk peranti anda!';
$lang['Experience The Wonders of Windows 10 Education'] = 'Alami Keajaiban Yang Windows 10 Education Tawarkan';
$lang['CLAIM YOUR ACCESS'] = 'DAPATKAN AKSES!';
$lang['windows10-writeup'] = "Windows 10 Education merupakan versi sistem operasi Windows yang paling padat bagi Microsoft. Ianya direka khas buat para pelajar dan juga bersesuaian dengan ruang kerja. Nikmati menu mula baru, pelayar Edge yang hebat, keselamatan yang diperbaharui dan banyak lagi. <br> Apa yang perlu anda lakukan hanyalah pastikan anda merupakan pelajar kerjasama universiti dan tuntu akses dari MUSE. Nikmati pengalaman bersama Windows 10 Education.";
$lang['Familiar Features'] = 'Fungsi yang Lazim';
$lang['Verified for Security'] = 'Keselamatan yang Sah';
$lang['Great Perfomance'] = 'Prestasi yang Hebat';
$lang['Easy Setup'] = 'Penetapan yang Mudah';
$lang['familiar-features-writeup'] = 'Alami inovasi yang terbaru dan tersedia dengan pemasangan penuh perisian-perisian Office dalam peranti anda';
$lang['verified-security-writeup'] = 'Keselamatan yang ringkas dan mudah. Perisian akan dipilih dan dihantar dari Stor Microsoft for Education kepada anda.';
$lang['great-perfomance-writeup'] = 'Direka untuk memberi prestasi yang selaras dengan kelas pembelajaran. Pembinaan adalah mudah dan cepat.';
$lang['easy-setup-writeup'] = 'Sambungan awan yang mudah diurus bersampingan edisi lain bagi Windows 10 dengan Penetapan Percuma PC Sekolah.';

//Verify Profile
$lang['titlecompleteprofile'] = 'Sahkan Akaun Anda';
$lang['completeprofilemsg'] = 'Sebelum anda menikmati semua faedah-faedah pelajar dari MUSE, and perlulah sahkan bahawa anda merupakan seorang pelajar.';

// Home
$lang['home-title'] = 'Tawaran Hebat Untuk Pelajar';
$lang['subhead-home'] = 'Jimat sehingga RM 50 000 untuk perisian dan latihan kami bawakan khas untuk para pelajar';
$lang['Join Now!'] = 'DAPATKAN SEKARANG!';
$lang['ourpartner'] = 'KOLABORASI<br>ANTARA';
$lang['getwithmusetitle'] = 'Tawaran hebat daripada MUSE';
$lang['Free Software'] = 'PERISIAN PERCUMA';
$lang['freesoftware-txt'] = 'Dapatkan perisian bernilai lebih daripada RM 50000 secara percuma';
$lang['Learn for free'] = 'PEMBELAJARAN PERCUMA';
$lang['learnfree-txt'] = 'Dapatkan akses kepada kursus premium di talian secara percuma';
$lang['Explore Career'] = 'TEROKAI KARIER';
$lang['career-txt'] = 'Terokai dunia karier anda dengan kemahiran baharu';

$lang['column1-title'] = 'Dapatkan alatan pembelajaran terbaik';
$lang['column1-content'] = 'Kami bawakan kepada anda peralatan pembelajaran tebaik untuk produktiviti pembelajaran anda. Daripada produktiviti kepada perisian rekaan. Kami bawakan perisian untuk membantu anda dalam proses pembelajaran';
$lang['column1-content2'] = 'Lebih daripada RM 50000 perisian untuk dimiliki';
$lang['column2-title'] = 'Persiapan untuk karier anda';
$lang['column2-content'] = 'Dapatkan latihan dan pengiktirafan bagi persiapan melengkapkan diri dengan kemahiran dalam permintaan industri. Melalui kerjasama dengan Microsoft Imagine Academy, impian kini jadi realiti';
$lang['column2-content2'] = '758 kursus tersedia - percuma kepada semua ahli MUSE';
$lang['column3-title'] = 'Terokai Dunia Karier';
$lang['column3-content'] = 'Bukan sahaja kami melatih dan menyediakan anda peralatan pembelajaran terbaik untuk karier anda, kami juga menyediakan platform untuk anda ceburi bidang karier pilihan anda.';
$lang['column3-content2'] = 'Lebih daripada 5000 peluang pekerjaan - hanya untuk ahli MUSE';

$lang['howtostart'] = 'Langkah-langkah untuk mendapatkan tawaran ini';
$lang['registerwithmuse'] = 'Daftar dengan MUSE';
$lang['verifystudentstatus'] = 'Sahkan status pelajar';
$lang['studentbenefit'] = 'Nikmati tawaran kami';

$lang['homeslider-title'] = 'Mulakan Kerjaya Anda Sekarang Bersama Kami!';
$lang['homeslider-title1'] = 'Jurutera Azure Cloud';
$lang['homeslider-center1'] = 'Microsoft Malaysia<br>KLCC, Kuala Lumpur<br>Gaji: RM 3,000';
$lang['homeslider-title2'] = 'Penganalisis Pemaju 3D';
$lang['homeslider-center2'] = 'Autodesk Malaysia<br>Axiata Tower, Kuala Lumpur<br>Gaji: RM 5,000';
$lang['homeslider-title3'] = 'Jurutera Sharepoint';
$lang['homeslider-center3'] = 'Microsoft Malaysia<br>KLCC, Kuala Lumpur<br>Gaji: RM 4,000';

$lang['allbenefit-title'] = 'SEMUA INI, DALAM MUSE';
$lang['GET STARTED!'] = 'MULA!';

$lang['CLICK HERE'] = 'KLIK DI SINI';
$lang['attention-title'] = 'KEPADA PELAJAR-PELAJAR ANTARABANGSA!';
$lang['attention-list1'] = 'Perlindungan Insurans Kesihatan?';
$lang['attention-list2'] = 'Bantuan Kebajikan Dan Hal Ehwal Pelajar?';
$lang['attention-list3'] = 'Pembaharuan VISA/iKad?';
$lang['attention-list4'] = 'Khidmat Pelanggan?';

// Events
$lang['eventhappening'] = 'Acara Berlangsung';
$lang['newsevents'] = 'Berita dan Acara';
$lang['allevent'] = 'Nantikan Acara-acara Yang Menarik Ini!';
$lang['btn-news-more'] = 'Berita Lanjut';
$lang['btn-events-more'] = 'Acara Lanjut';
$lang['time'] = 'WAKTU';
$lang['date'] = 'TARIKH';
$lang['fee'] = 'PEMBAYARAN';
$lang['category'] = 'Kategori';
$lang['when'] = 'Masa';
$lang['All Events Happening'] = 'Semua Acara';
$lang['This Month'] = 'Bulan Ini';
$lang['allnews'] = 'Semua Berita';

// Days
$lang['Monday'] = 'ISNIN';
$lang['Tuesday'] = 'SELASA';
$lang['Wednesday'] = 'RABU';
$lang['Thursday'] = 'KHAMIS';
$lang['Friday'] = 'JUMAAT';
$lang['Saturday'] = 'SABTU';
$lang['Sunday'] = 'AHAD';

// Adobe
$lang['Adobe'] = 'Adobe';
$lang['subhead-adobe'] = 'Akan Datang!';
$lang['adobe-title1'] = 'PERALATAN WEB';
$lang['adobe-content1'] = 'Transform idea-idea bernas anda dengan peralatan reka bentuk sesawang yang luar biasa dari kami! Bina laman sesawang yang moden tanpa pengekodan dan pilih dari lebih ribuan fon muka taip untuk memberi wajah baru yang hebat untuk laman web anda.';
$lang['adobe-title2'] = 'PERALATAN REKA BENTUK WEB';
$lang['adobe-content2'] = 'Peralatan reka bentuk sesawang yang terkemuka di dunia - Creative Cloud kini memberi anda kelebihan untuk sentiasa menjadi kreatif dalam menghasilkan karya impian anda.  Gunakan perisian yang kami tawarkan untuk malakar, melukis, dan membina rekasusunan laman sesawang anda.';
$lang['adobe-title3'] = 'PERALATAN VIDEO DAN AUDIO';
$lang['adobe-content3'] = 'Dari tayangan di rumah, dan video-video Youtube, ke tayangan hebat blokbuster Hollywood - Creative Cloud sedia membantu anda! Reka grafik, efek animasi, dan audio yng berkualiti. Desktop dan juga perisian telefon pintar anda boleh bekerjasama dengan mudah. Anda kini boleh bawa karya susulan filem anda ke rancangan television, dan juga laman sesawang.';

// Autodesk
$lang['Autodesk'] = 'Autodesk';
$lang['subhead-autodesk'] = 'Akan Datang!';

//Form Validation
$lang['valid-email'] = 'Emel diperlukan!';
$lang['valid-pass'] = 'Kata Laluan diperlukan!';
$lang['valid-email-pattern'] = 'Emel tidak sah!';
$lang['emailpass-wrong'] = 'Emel / Kata Laluan tidak sah!';
$lang['valid-ic'] = 'NRIC diperlukan!';
$lang['valid-phone'] = 'Nombor Telefon diperlukan!';
$lang['inst-email-required'] = 'Emel Institusi diperlukan!';
$lang['inst-name-required'] = 'Nama Institusi diperlukan!';
$lang['warning-msg-form'] = 'Sila isi maklumat dibawah!';
$lang['success-msg-form'] = 'Berjaya dikemas kini!';
$lang['curpass-wrong-form'] = 'Kata laluan asal tidak salah!';
$lang['newpass-wrong-form'] = 'Kata laluan baru tidak sepadan!';
$lang['newpass-less-form'] = 'Kata laluan mestilah sekurang-kurangnya 8 aksara dan mengandungi perkara berikut: huruf besar (A-Z), huruf kecil (a-z), nombor (0-9) dan aksara khusus (cth.! @ # $% ^ & *)';
$lang['valid-fullname'] = 'Nama Lengkap diperlukan!';

$lang['sortby'] = 'Cari melalui';
$lang['nameofinst'] = 'Nama Institusi';
$lang['location'] = 'Lokasi';
$lang['Find Your Institution Here'] = 'Cari Institusi Anda Di Sini';
$lang['emaillist'] = 'Emel';
$lang['contactno'] = 'Hubungi';

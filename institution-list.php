<?php
include 'header.php';
?>
  <main id="main">
  <section id="instlist">
    <div class="container">
      <div class="content">
        <h3><?php echo $lang['Find Your Institution Here']; ?></h3>
        <p class="normal-txt"><?php echo $lang['sortby']; ?></p>
        <div class="instlist-filter">
          <div class="container">
            <select class="instname" name="inst-name" >
              <option value=""><?php echo $lang['nameofinst'];?></option>
            </select>
            <select class="location" name="location" >
              <option value=""><?php echo $lang['location'];?></option>
            </select>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-4 instlistbox" style="padding-right: 5px;">
            <h5>Politeknik Sultan Salahuddin Abdul Aziz Shah</h5>
            <p class="normal-txt">Shah Alam, Selangor</p>
            <p class="normal-txt"><?php echo $lang['emaillist'];?>: <a href="mailto:support@polysaaz.edu.my">support@polysaaz.edu.my</a></p>
            <p class="normal-txt"><?php echo $lang['contactno'];?>: 03-5163 4000</p>
          </div>

          <div class="col-lg-4 instlistbox" style="padding-right: 5px;">
          <h5>Politeknik Sultan Salahuddin Abdul Aziz Shah</h5>
            <p class="normal-txt">Shah Alam, Selangor</p>
            <p class="normal-txt"><?php echo $lang['emaillist'];?>: <a href="mailto:support@polysaaz.edu.my">support@polysaaz.edu.my</a></p>
            <p class="normal-txt"><?php echo $lang['contactno'];?>: 03-5163 4000</p>
          </div>

          <div class="col-lg-4 instlistbox" style="padding-right: 5px;">
          <h5>Politeknik Sultan Salahuddin Abdul Aziz Shah</h5>
            <p class="normal-txt">Shah Alam, Selangor</p>
            <p class="normal-txt"><?php echo $lang['emaillist'];?>: <a href="mailto:support@polysaaz.edu.my">support@polysaaz.edu.my</a></p>
            <p class="normal-txt"><?php echo $lang['contactno'];?>: 03-5163 4000</p>
          </div>

        </div>
      </div>
    </div>
  </section><!-- #writeup -->

  </main>

<?php
include 'footer.php';
?>
<?php

require_once 'config.php';

function getDBC() {
    $dbc = mysqli_connect(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
    if(!$dbc) die("Unable to connect to MySQL: " . mysqli_error($dbc));
    return $dbc;
}

function getObject($data) {
    return mysqli_fetch_object($data);
}


?>
<?php
session_start();
include('api/httpful.phar');

$userdata = '';
$errMsg = '';
$dataRespond = '';
$_SESSION["message"] = '';
$_SESSION["error"] = '';
$_SESSION["messageAcc"] = '';
$_SESSION["errorAcc"] = '';
$_SESSION['messagePass'] = '';
$_SESSION['messagePassForgot'] = '';
$_SESSION["successPassForgot"] = '';
$_SESSION["messageServer"] = '';
$_SESSION["successVerify"] = '';
$_SESSION["errorVerify"] = '';
$_SESSION["successAcc"] = '';
$_SESSION["successregister"] = FALSE;

function sanitize($data) {
    $data = trim($data);
    $data = htmlspecialchars($data);
    return $data;
}

/* User Login */
if ( isset($_POST['submit-login']) ) {
    // Sample Data
    $emailDebug = 'mshuhaileyfx@gmail.com';
    $passDebug = '123qwe';

    $email = $_POST['email'];
    $pass = $_POST['password'];
    $_SESSION["CURRENT_PASS"] = $pass;
    $_SESSION["current_email"] = $email;

    //use this for now
    if ( !empty($email) AND !empty($pass) ) {
        //REST API for Login
        $uri = 'https://educloud-account-api-development.azurewebsites.net/connect/token';
        $response = \Httpful\Request::post($uri)
            ->addHeader('Content-Type', 'application/x-www-form-urlencoded') 
            ->body('client_id=EduCloudClient&username='.$email.'&password='.$pass.'&scope=EduCloudIdAPI offline_access&grant_type=password&client_secret=secret')    
            ->send();   
        $response = json_decode($response);

        foreach ( $response as $key => $val ) {
            if ( $key == 'error' ) {
                $errorRespond = $val;
                $method = 'error';
            }

            if ( $key == 'access_token' ) {
                $accessToken = $val;
                $method = 'access_token';
                $_SESSION["access_token"] = trim($val);
            }
            if ( $key == 'expires_in' ) {
                $expireToken = $val;
            }
            if ( $key == 'refresh_token' ) {
                $refreshToken = $val;
            }
        }

        if ( $method == 'access_token' ) {

            //setcookie('access_token', $accessToken, $expireToken, "/");
            $_SESSION["allowedlogin"] = TRUE;
            $_SESSION['LAST_ACTIVITY'] = time();
            // Get User data
            $uriProfile = 'https://educloud-profile-api-development.azurewebsites.net/me/Profile';
            $responseProfile = \Httpful\Request::get($uriProfile)
                ->addHeader('accept', 'application/json') 
                ->addHeader('Authorization', 'Bearer '.$_SESSION["access_token"].'')    
                ->send();   
            $responseProfile = json_decode($responseProfile);
            $_SESSION["alldata"] = $responseProfile;
            foreach ( $responseProfile as $keyProfile => $valProfile ) {
                if ( $keyProfile == 'personalInfoId' ) $personalInfoId = $valProfile;
                if ( $keyProfile == 'fullName' ) $fullName = $valProfile;
                if ( $keyProfile == 'email' ) $emailProfile = $valProfile;
                if ( $keyProfile == 'dateOfBirth' ) $dateOfBirth = $valProfile;
                if ( $keyProfile == 'icpassportNo' ) $icpassportNo = $valProfile;
                if ( $keyProfile == 'mobile' ) $mobile = $valProfile;
                if ( $keyProfile == 'gender' ) $gender = $valProfile;
                if ( $keyProfile == 'schoolEmail' ) $schoolEmail = $valProfile;
                if ( $keyProfile == 'insitutionName' ) $insitutionName = $valProfile;
                if ( $keyProfile == 'isSchoolEmailActivated' ) $isSchoolEmailActivated = $valProfile;
            }

            $_SESSION["userdata"] = array(
                'personalInfoId'    => $personalInfoId,
                'fullName'          => $fullName,
                'email'             => $emailProfile,
                'dateOfBirth'       => $dateOfBirth,
                'icpassportNo'      => $icpassportNo,
                'mobile'            => $mobile,
                'gender'            => $gender,
                'schoolEmail'       => $schoolEmail,
                'insitutionName'    => $insitutionName,
                'isSchoolEmailActivated'    => $isSchoolEmailActivated
            );

        $userdata = $_SESSION["userdata"];
        $_SESSION["allowedlogin"] = TRUE;
        
        if ( $_SESSION["userdata"]['fullName'] != NULL 
            AND $_SESSION["userdata"]['dateOfBirth'] != NULL 
            AND $_SESSION["userdata"]['icpassportNo'] != NULL
            AND $_SESSION["userdata"]['mobile'] != NULL
            AND $_SESSION["userdata"]['gender'] != NULL
            AND $_SESSION["userdata"]['insitutionName'] != NULL
            AND $_SESSION["userdata"]['email'] != NULL
            AND $_SESSION["userdata"]['schoolEmail'] != NULL
            AND $_SESSION["userdata"]['isSchoolEmailActivated'] == TRUE
            ) {
                header("Location: index");
                exit;
            } else if ( $_SESSION["userdata"]['isSchoolEmailActivated'] == FALSE ) {
                header("Location: settings");
                exit;
            } else {
                header("Location: settings");
                exit;
            }

        } else {
            $_SESSION["allowedlogin"] = FALSE;
            $_SESSION["message"] = $lang['emailpass-wrong'];
            $_SESSION["error"] = $dataRespond;
        }
        
    } else {
        $_SESSION["allowedlogin"] = FALSE;
        $_SESSION["message"] = $lang['emailpass-wrong'];
    }
}

/* Update User Profile */
if ( isset($_POST['update-profile']) ) {

    $fullName = sanitize($_POST['fullName']);
    $dateOfBirth = sanitize($_POST['dateOfBirth']);
    $icpassportNo = sanitize($_POST['icpassportNo']);
    $mobile = sanitize($_POST['mobile']);
    $gender = sanitize($_POST['gender']);
    $insitutionName = sanitize($_POST['insitutionName']);

    //use this for now
    if ( !empty($fullName) AND !empty($dateOfBirth) AND !empty($icpassportNo) AND !empty($mobile) AND !empty($gender) AND !empty($insitutionName) ) {
        //Update Profile
        $uriProfileUpdate = 'https://educloud-profile-api-development.azurewebsites.net/me/Profile';
        $responseProfileUpdate = \Httpful\Request::put($uriProfileUpdate)
            ->addHeader('accept', 'application/json') 
            ->addHeader('Authorization', 'Bearer '.$_SESSION["access_token"].'')    
            ->addHeader('Content-Type', 'application/json-patch+json')
            ->body('{
            "icpassportNo" : "'.$icpassportNo.'",
            "title" : "",
            "fullName" : "'.$fullName.'",
            "firstName" : "",
            "middleName" : "",
            "lastName" : "",
            "nationality" : "Malaysia",
            "dateOfBirth" : "'.$dateOfBirth.'",
            "gender" : "'.$gender.'",
            "mobile" : "'.$mobile.'",
            "email" : "'.$_SESSION["current_email"].'",
            "background" : "",
            "insitutionName" : "'.$insitutionName.'"
            }')    
            ->send();   
        $responseProfileUpdate = json_decode($responseProfileUpdate);
        foreach ($responseProfileUpdate as $keyProfile => $valProfile) {
            if ( $keyProfile == 'message' ) $respondMessage = $valProfile;
            if ( $keyProfile == 'errors' ) $infoError = $valProfile;
        }

        if ( $respondMessage == 'Validation Failed' ) {
            $_SESSION["message"] = $respondMessage.' : '.$infoError[0]->field;

        } else {
            // Get User data
            $uriProfile = 'https://educloud-profile-api-development.azurewebsites.net/me/Profile';
            $responseProfile = \Httpful\Request::get($uriProfile)
                ->addHeader('accept', 'application/json') 
                ->addHeader('Authorization', 'Bearer '.$_SESSION["access_token"].'')    
                ->send();   
            $responseProfile = json_decode($responseProfile);
            
            foreach ( $responseProfile as $keyProfile => $valProfile ) {
                if ( $keyProfile == 'personalInfoId' ) $personalInfoId = $valProfile;
                if ( $keyProfile == 'fullName' ) $fullName = $valProfile;
                if ( $keyProfile == 'email' ) $emailProfile = $valProfile;
                if ( $keyProfile == 'dateOfBirth' ) $dateOfBirth = $valProfile;
                if ( $keyProfile == 'icpassportNo' ) $icpassportNo = $valProfile;
                if ( $keyProfile == 'mobile' ) $mobile = $valProfile;
                if ( $keyProfile == 'gender' ) $gender = $valProfile;
                if ( $keyProfile == 'schoolEmail' ) $schoolEmail = $valProfile;
                if ( $keyProfile == 'insitutionName' ) $insitutionName = $valProfile;
                if ( $keyProfile == 'isSchoolEmailActivated' ) $isSchoolEmailActivated = $valProfile;
            }

            $_SESSION["userdata"] = '';
            unset($_SESSION["userdata"]);

            $_SESSION["userdata"] = array(
                'personalInfoId'    => $personalInfoId,
                'fullName'          => $fullName,
                'email'             => $emailProfile,
                'dateOfBirth'       => $dateOfBirth,
                'icpassportNo'      => $icpassportNo,
                'mobile'            => $mobile,
                'gender'            => $gender,
                'schoolEmail'       => $schoolEmail,
                'insitutionName'    => $insitutionName,
                'isSchoolEmailActivated'    => $isSchoolEmailActivated
            );
        }

        $_SESSION["success"] = $lang['success-msg-form'];
        
    } else {
        $_SESSION["message"] = $lang['warning-msg-form'];
    }
}

/* Update User Profile */
if ( isset($_POST['update-accountinfo']) ) {

    $schoolEmail = sanitize($_POST['schoolEmail']);

    //use this for now
    if ( !empty($schoolEmail) ) {
        //Update Profile
        $uriAccUpdate = 'https://educloud-profile-api-development.azurewebsites.net/me/SchoolEmail';
        $responseAccUpdate = \Httpful\Request::patch($uriAccUpdate)
            ->addHeader('accept', 'application/json') 
            ->addHeader('Authorization', 'Bearer '.$_SESSION["access_token"].'')    
            ->addHeader('Content-Type', 'application/json-patch+json')
            ->body('{
            "schoolEmail" : "'.$schoolEmail.'",
            "activationURL" : "'.BASE_URL.'verify-school-email"
            }')    
            ->send();   
        $responseAccUpdate = json_decode($responseAccUpdate);
        
        foreach ($responseAccUpdate as $keyProfile => $valProfile) {
            if ( $keyProfile == 'message' ) $respondMessage = $valProfile;
            if ( $keyProfile == 'errors' ) $infoError = $valProfile;
        }

        if ( $respondMessage == 'Validation Failed' ) {
            $_SESSION["messageAcc"] = $respondMessage.' : '.$infoError[0]->field;

        } else {
            // Get User data
            $uriProfile = 'https://educloud-profile-api-development.azurewebsites.net/me/Profile';
            $responseProfile = \Httpful\Request::get($uriProfile)
                ->addHeader('accept', 'application/json') 
                ->addHeader('Authorization', 'Bearer '.$_SESSION["access_token"].'')    
                ->send();   
            $responseProfile = json_decode($responseProfile);
            $_SESSION["responseProfile"] = $responseProfile;
            foreach ( $responseProfile as $keyProfile => $valProfile ) {
                if ( $keyProfile == 'personalInfoId' ) $personalInfoId = $valProfile;
                if ( $keyProfile == 'fullName' ) $fullName = $valProfile;
                if ( $keyProfile == 'email' ) $emailProfile = $valProfile;
                if ( $keyProfile == 'dateOfBirth' ) $dateOfBirth = $valProfile;
                if ( $keyProfile == 'icpassportNo' ) $icpassportNo = $valProfile;
                if ( $keyProfile == 'mobile' ) $mobile = $valProfile;
                if ( $keyProfile == 'gender' ) $gender = $valProfile;
                if ( $keyProfile == 'schoolEmail' ) $schoolEmail = $valProfile;
                if ( $keyProfile == 'insitutionName' ) $insitutionName = $valProfile;
                if ( $keyProfile == 'isSchoolEmailActivated' ) $isSchoolEmailActivated = $valProfile;
                if ( $keyProfile == 'schoolEmailActivationCode' ) $schoolEmailActivationCode = $valProfile;
            }

            $_SESSION["userdata"] = '';
            unset($_SESSION["userdata"]);

            $_SESSION["userdata"] = array(
                'personalInfoId'    => $personalInfoId,
                'fullName'          => $fullName,
                'email'             => $emailProfile,
                'dateOfBirth'       => $dateOfBirth,
                'icpassportNo'      => $icpassportNo,
                'mobile'            => $mobile,
                'gender'            => $gender,
                'schoolEmail'       => $schoolEmail,
                'insitutionName'    => $insitutionName,
                'isSchoolEmailActivated'    => $isSchoolEmailActivated,
                'schoolEmailActivationCode'    => $schoolEmailActivationCode
            );

            $_SESSION["successAcc"] = $lang['send-email-verify'];

            /*$to      = $email;
            $subject = 'MUSE Admin -'.$lang['verify-email-header'];
            
            $headers = "From: " . strip_tags(ADMIN_EMAIL) . "\r\n";
            $headers .= "Reply-To: ". strip_tags(ADMIN_EMAIL) . "\r\n";
            $headers .= "MIME-Version: 1.0\r\n";
            $headers .= "Content-Type: text/html; charset=UTF-8\r\n";

            $message = 'Hi '.$fullName.',<br><br>Please find activation code below<br>'.$schoolEmailActivationCode.'<br><br>To verify your email, please click link below<br> <a href="'.BASE_URL.'verify-school-email?e='.$emailProfile.'&vc='.$schoolEmailActivationCode.'">'.BASE_URL.'verify-school-email?e='.$emailProfile.'&vc='.$schoolEmailActivationCode.'</a><br><br>Thanks,<br>-- Admin';

            $sendEmail = mail($to, $subject, $message, $headers);
            
            if ( $sendEmail ) {
                $_SESSION["successAcc"] = $lang['send-email-verify'];
            } else {
                $_SESSION["messageAcc"] = 'Error Sending Email! Please check your server.';
            }*/
            
        }
        
    } else {
        $_SESSION["messageAcc"] = $lang['warning-msg-form'];
    }
}

/* Update Pass User */
if ( isset($_POST['update-passinfo']) ) {

    $currentPass = sanitize($_POST['current-pass']);
    $newPass = sanitize($_POST['new-pass']);
    $newPassRepeat = sanitize($_POST['new-pass-repeat']);
    $proceed = TRUE;
    $pattern = ' ^.*(?=.{7,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).*$ ';

    //use this for now
    if ( !empty($currentPass) AND !empty($newPass) AND !empty($newPassRepeat) ) {
        if ( $currentPass != $_SESSION["CURRENT_PASS"] ) {
            $_SESSION["messagePass"] = $lang['curpass-wrong-form'];
            $proceed = FALSE;
        }

        if ( $newPass != $newPassRepeat ) {
            $_SESSION["messagePass"] = $lang['newpass-wrong-form'];
            $proceed = FALSE;
        }

        if ( strlen($newPass) < 8 AND !preg_match($pattern,$newPass) ) {
            $_SESSION["messagePass"] = $lang['newpass-less-form'];
            $proceed = FALSE;
        }

        if ( $proceed === TRUE ) {
            //Update Profile
            $data = '?username='.$_SESSION["userdata"]['email'].'&OldPassword='.$_SESSION["CURRENT_PASS"].'&NewPassword='.$newPass.'';
            $uriAccUpdate = 'https://educloud-account-api-development.azurewebsites.net/account/UpdateUserPassword'.$data;
            $responseAccUpdate = \Httpful\Request::post($uriAccUpdate) 
                ->addHeader('Content-Type', 'application/x-www-form-urlencoded')    
                ->send();   
            $responseAccUpdate = json_decode($responseAccUpdate);
            
            foreach ($responseAccUpdate as $keyProfile => $valProfile) {
                if ( $keyProfile == 'message' ) $respondMessage = $valProfile;
                if ( $keyProfile == 'errors' ) $infoError = $valProfile;
            }

            if ( $respondMessage == 'Validation Failed' ) {
                $_SESSION["messagePass"] = $respondMessage.' : '.$infoError[0]->field;

            } else {
                $_SESSION["successPass"] = $lang['success-msg-form'];
            }
        }
        
    } else {
        $_SESSION["messagePass"] = $lang['warning-msg-form'];
    }
}

/* Update Pass User */
if ( isset($_POST['forgot-pass']) ) {

    $email = sanitize($_POST['forgot-pass-email']);
    $proceed = TRUE;

    //use this for now
    if ( !empty($email) ) {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $_SESSION["messagePassForgot"] = $lang['valid-email-pattern'];
            $proceed = FALSE;
        }

        if ( $proceed === TRUE ) {
            //Update Profile
            $data = '?email='.$email.'';
            $uriAccUpdate = 'https://educloud-account-api-development.azurewebsites.net/account/ForgotUserPassword'.$data;
            $responseAccUpdate = \Httpful\Request::post($uriAccUpdate) 
                ->addHeader('Content-Type', 'application/x-www-form-urlencoded')    
                ->send();   
            $responseAccUpdate = json_decode($responseAccUpdate);
            
            foreach ($responseAccUpdate as $keyProfile => $valProfile) {
                if ( $keyProfile == 'message' ) $respondMessage = $valProfile;
                if ( $keyProfile == 'errors' ) $infoError = $valProfile;
                if ( $keyProfile == 'details' ) $details = $valProfile;
            }

            if ( $respondMessage == 'Email not confirm' ) {
                $_SESSION["messagePassForgot"] = $infoError[0]->message;
            } else if ( $respondMessage == 'Not Found' ) {
                $_SESSION["messagePassForgot"] = $infoError[0]->message;
            } else if ( $respondMessage == 'Success' ) {
                $_SESSION["successPassForgot"] = $respondMessage.'. '.$details[0]->message;
            }
        }
        
    } else {
        $_SESSION["messagePassForgot"] = $lang['warning-msg-form'];
    }
}

/* User Signup */
if ( isset($_POST['submit-signup']) ) {

    $fullname = sanitize($_POST['fullname']);
    $email = sanitize($_POST['email']);
    $pass = sanitize($_POST['password']);
    $proceed = TRUE;
    $pattern = ' ^.*(?=.{7,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).*$ ';

    if ( strlen($pass) < 8 AND !preg_match($pattern, $pass) ) {
        $_SESSION["messagePass"] = $lang['newpass-less-form'];
        $proceed = FALSE;
    }

    if ( $proceed == TRUE ) {
        // Register in session
        $_SESSION['fullname_signup'] = $fullname;
        $_SESSION['fullname_email'] = $email;
        $_SESSION['fullname_pass'] = $pass;

        $uriRegister = 'https://educloud-profile-api-development.azurewebsites.net/RegisterMuse';
        $responseRegister = \Httpful\Request::post($uriRegister)
        ->addHeader('accept', 'application/json') 
        ->addHeader('Content-Type', 'application/json-patch+json')
        ->body('{ 
        "activationURL": "http://www.educloud.my", 
        "email": "'.$_SESSION['fullname_email'].'", 
        "password": "'.$_SESSION['fullname_pass'].'", 
        "name": "'.$_SESSION['fullname_signup'].'", 
        "displayName": "'.$_SESSION['fullname_signup'].'" }')    
        ->send();   
        $responseRegister = json_decode($responseRegister);

        foreach ( $responseRegister as $key => $val ) {
            if ( $key == 'errors' ) {
                $infoError = $val;
                $method = 'errors';
            }

            if ( $key == 'message' ) {
                $errorRespond = $val;
                $method = 'errors';
            }

            if ( $key == 'access_token' ) {
                $accessToken = $val;
                $method = 'access_token';
                $_SESSION["access_token"] = trim($val);
            }
            if ( $key == 'expires_in' ) {
                $expireToken = $val;
            }
            if ( $key == 'refresh_token' ) {
                $refreshToken = $val;
            }
        }

        if ( $errorRespond == 'Validation Failed' ) {
            $_SESSION["messageServer"] = $errorRespond.' : '.$infoError[0]->message;

        } 
        
        if ( $method == 'access_token' ) {

            //setcookie('access_token', $accessToken, $expireToken, "/");
            $_SESSION["allowedlogin"] = TRUE;
            $_SESSION['LAST_ACTIVITY'] = time();
            // Get User data
            $uriProfile = 'https://educloud-profile-api-development.azurewebsites.net/me/Profile';
            $responseProfile = \Httpful\Request::get($uriProfile)
                ->addHeader('accept', 'application/json') 
                ->addHeader('Authorization', 'Bearer '.$_SESSION["access_token"].'')    
                ->send();   
            $responseProfile = json_decode($responseProfile);

            foreach ( $responseProfile as $keyProfile => $valProfile ) {
                if ( $keyProfile == 'personalInfoId' ) $personalInfoId = $valProfile;
                if ( $keyProfile == 'fullName' ) $fullName = $valProfile;
                if ( $keyProfile == 'email' ) $emailProfile = $valProfile;
                if ( $keyProfile == 'dateOfBirth' ) $dateOfBirth = $valProfile;
                if ( $keyProfile == 'icpassportNo' ) $icpassportNo = $valProfile;
                if ( $keyProfile == 'mobile' ) $mobile = $valProfile;
                if ( $keyProfile == 'gender' ) $gender = $valProfile;
                if ( $keyProfile == 'schoolEmail' ) $schoolEmail = $valProfile;
                if ( $keyProfile == 'insitutionName' ) $insitutionName = $valProfile;
                if ( $keyProfile == 'isSchoolEmailActivated' ) $isSchoolEmailActivated = $valProfile;
            }

            $_SESSION["userdata"] = array(
                'personalInfoId'    => $personalInfoId,
                'fullName'          => $fullName,
                'email'             => $emailProfile,
                'dateOfBirth'       => $dateOfBirth,
                'icpassportNo'      => $icpassportNo,
                'mobile'            => $mobile,
                'gender'            => $gender,
                'schoolEmail'       => $schoolEmail,
                'insitutionName'    => $insitutionName,
                'isSchoolEmailActivated'    => $isSchoolEmailActivated
            );

        $userdata = $_SESSION["userdata"];
        $_SESSION["allowedlogin"] = TRUE;
        $_SESSION["successregister"] = TRUE;

        }

    }

}

/* User Signup */
if ( isset($_POST['submit-extra']) ) {

    $icnumber1 = sanitize($_POST['ic-number1']);
    $icnumber2 = sanitize($_POST['ic-number2']);
    $icnumber3 = sanitize($_POST['ic-number3']);
    $nric = $icnumber1.''.$icnumber2.''.$icnumber3;
    $phone = sanitize($_POST['phone']);

    // Register in session
    $_SESSION['nric'] = $nric;
    $_SESSION['phone'] = $phone;
    
    //header("Location: verify-account");
    //exit;

}

/* Update User Profile */
if ( isset($_POST['verify-account']) ) {

    $schoolEmail = sanitize($_POST['institution-email']);
    $verificationCode = sanitize($_POST['verify-code']);

    //use this for now
    if ( !empty($schoolEmail) AND !empty($verificationCode)) {
        //Activate Profile
        $uriAccUpdate = 'https://educloud-profile-api-development.azurewebsites.net/me/ActivateSchoolEmail';
        $responseAccUpdate = \Httpful\Request::patch($uriAccUpdate)
            ->addHeader('accept', 'application/json') 
            ->addHeader('Authorization', 'Bearer '.$_SESSION["access_token"].'')    
            ->addHeader('Content-Type', 'application/json-patch+json')
            ->body('{
            "schoolEmail" : "'.$schoolEmail.'",
            "activationCode" : "'.$verificationCode.'"
            }')    
            ->send();   
        $responseAccUpdate = json_decode($responseAccUpdate);

        foreach ( $responseAccUpdate as $keyProfile => $valProfile) {
            if ( $keyProfile == 'message' ) $respondMessage = $valProfile;
            if ( $keyProfile == 'errors' ) $infoError = $valProfile;
        }

        if ( $respondMessage == 'Validation Failed' ) {
            $_SESSION["errorVerify"] = $respondMessage.' : '.$infoError[0]->field;

        } else {
            // Get User data
            /*$uriProfile = 'https://educloud-profile-api-development.azurewebsites.net/me/Profile';
            $responseProfile = \Httpful\Request::get($uriProfile)
                ->addHeader('accept', 'application/json') 
                ->addHeader('Authorization', 'Bearer '.$_SESSION["access_token"].'')    
                ->send();   
            $responseProfile = json_decode($responseProfile);

            foreach ( $responseProfile as $keyProfile => $valProfile ) {
                if ( $keyProfile == 'personalInfoId' ) $personalInfoId = $valProfile;
                if ( $keyProfile == 'fullName' ) $fullName = $valProfile;
                if ( $keyProfile == 'email' ) $emailProfile = $valProfile;
                if ( $keyProfile == 'dateOfBirth' ) $dateOfBirth = $valProfile;
                if ( $keyProfile == 'icpassportNo' ) $icpassportNo = $valProfile;
                if ( $keyProfile == 'mobile' ) $mobile = $valProfile;
                if ( $keyProfile == 'gender' ) $gender = $valProfile;
                if ( $keyProfile == 'schoolEmail' ) $schoolEmail = $valProfile;
                if ( $keyProfile == 'insitutionName' ) $insitutionName = $valProfile;
                if ( $keyProfile == 'isSchoolEmailActivated' ) $isSchoolEmailActivated = $valProfile;
                if ( $keyProfile == 'schoolEmailActivationCode' ) $schoolEmailActivationCode = $valProfile;
            }

            $_SESSION["userdata"] = '';
            unset($_SESSION["userdata"]);

            $_SESSION["userdata"] = array(
                'personalInfoId'    => $personalInfoId,
                'fullName'          => $fullName,
                'email'             => $emailProfile,
                'dateOfBirth'       => $dateOfBirth,
                'icpassportNo'      => $icpassportNo,
                'mobile'            => $mobile,
                'gender'            => $gender,
                'schoolEmail'       => $schoolEmail,
                'insitutionName'    => $insitutionName,
                'isSchoolEmailActivated'    => $isSchoolEmailActivated,
                'schoolEmailActivationCode'    => $schoolEmailActivationCode
            );*/

            session_unset();    
            session_destroy();
            
            $_SESSION["successVerify"] = $lang['successverifypending'];
            
        }
        
    } else {
        $_SESSION["errorVerify"] = $lang['warning-msg-form'];
    }
}




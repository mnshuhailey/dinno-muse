<?php

include 'database.php';

// To get list of News
function getNewsLatest()
{
    $dbc = getDBC();
    $newsListLatest = mysqli_query($dbc, "SELECT news_id, news_title_en, news_description_en, news_title_bm, news_description_bm, news_image FROM news ORDER BY news_id DESC LIMIT 1") or die("Error: ".mysqli_error($dbc));
    return $newsListLatest;
}

// Get all list News
function getNews()
{
    $dbc = getDBC();
    $newsList = mysqli_query($dbc, "SELECT news_id, news_title_en, news_description_en, news_title_bm, news_description_bm, news_image FROM news ORDER BY news_id DESC LIMIT 3") or die("Error: ".mysqli_error($dbc));
    return $newsList;
}

// Get News Details
function getNewsDetails($nid)
{
    $dbc = getDBC();
    $newsList = mysqli_query($dbc, "SELECT news_id, news_title_en, news_description_en, news_title_bm, news_description_bm, news_image, news_date_created FROM news WHERE news_id = '$nid'") or die("Error: ".mysqli_error($dbc));
    return $newsList;
}

// To get list of Latest Events
function getEventsLatest()
{
    $dbc = getDBC();
    $newsListLatest = mysqli_query($dbc, "SELECT events_id, events_title_en, events_description_en, events_title_bm, events_description_bm, events_image FROM events ORDER BY events_id DESC LIMIT 6") or die("Error: ".mysqli_error($dbc));
    return $newsListLatest;
}

// To get list of Latest Events
function getAllEvents()
{
    $dbc = getDBC();
    $newsListLatest = mysqli_query($dbc, "SELECT events_id, events_title_en, events_description_en, events_title_bm, events_description_bm, events_image, date_start, date_end, time_start, time_end, fee, date_created FROM events ORDER BY events_id DESC") or die("Error: ".mysqli_error($dbc));
    return $newsListLatest;
}

// To get list of Latest Events
function getAllEventsFilter($month, $year)
{
    $dbc = getDBC();
    $newsListLatest = mysqli_query($dbc, "SELECT events_id, events_title_en, events_description_en, events_title_bm, events_description_bm, events_image, date_start, date_end, time_start, time_end, fee, date_created FROM events WHERE MONTH(date_start) = $month AND YEAR(date_start) = $year ORDER BY events_id DESC") or die("Error: ".mysqli_error($dbc));
    return $newsListLatest;
}

// To get All News
function getAllNews()
{
    $dbc = getDBC();
    $newsList = mysqli_query($dbc, "SELECT news_id, news_title_en, news_description_en, news_title_bm, news_description_bm, news_image, news_date_created FROM news ORDER BY news_id DESC") or die("Error: ".mysqli_error($dbc));
    return $newsList;
}

// Crop image
function image_resize($src, $dst, $width, $height, $crop=0){

    if(!list($w, $h) = getimagesize($src)) return "Unsupported picture type!";
  
    $type = strtolower(substr(strrchr($src,"."),1));
    if($type == 'jpeg') $type = 'jpg';
    switch($type){
      case 'bmp': $img = imagecreatefromwbmp($src); break;
      case 'gif': $img = imagecreatefromgif($src); break;
      case 'jpg': $img = imagecreatefromjpeg($src); break;
      case 'png': $img = imagecreatefrompng($src); break;
      default : return "Unsupported picture type!";
    }
  
    // resize
    if($crop){
      //if($w < $width or $h < $height) return "Picture is too small!";
      $ratio = max($width/$w, $height/$h);
      $h = $height / $ratio;
      $x = ($w - $width / $ratio) / 2;
      $w = $width / $ratio;
    }
    else{
      if($w < $width and $h < $height) return "Picture is too small!";
      $ratio = min($width/$w, $height/$h);
      $width = $w * $ratio;
      $height = $h * $ratio;
      $x = 0;
    }
  
    $new = imagecreatetruecolor($width, $height);
  
    // preserve transparency
    if($type == "gif" or $type == "png"){
      imagecolortransparent($new, imagecolorallocatealpha($new, 0, 0, 0, 127));
      imagealphablending($new, false);
      imagesavealpha($new, true);
    }
  
    imagecopyresampled($new, $img, 0, 0, $x, 0, $width, $height, $w, $h);
  
    switch($type){
      case 'bmp': imagewbmp($new, $dst); break;
      case 'gif': imagegif($new, $dst); break;
      case 'jpg': imagejpeg($new, $dst); break;
      case 'png': imagepng($new, $dst); break;
    }
    return true;
}

?>
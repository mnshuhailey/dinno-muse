<?php
include 'header.php';
?>
  
  <main id="main">

    <!--==========================
      Verify Account Section
    ============================-->
    <section id="signup" class="wow verify-account">
      <div class="container">
        <div class="row">     
            <div class="tabBox">
              <h3><?php echo $lang['Verify your account']; ?></h3>
              <div class="tabContainer">
                <div id="signupform" class="tabContent">
                  <form name="verify-account" action="">
                    <select class="institution-name" id="inst-name" name="inst-name">
                      <option value=""><?php echo $lang['inst-name']; ?></option>
                      <option value="International Islamic University Malaysia (IIUM)">International Islamic University Malaysia (IIUM)</option>
                      <option value="Kolej Komuniti Bentong">Kolej Komuniti Bentong</option>
                      <option value="Kolej Komuniti Kuala Langat">Kolej Komuniti Kuala Langat</option>
                      <option value="Kolej Komuniti Selandar">Kolej Komuniti Selandar</option>
                      <option value="Politeknik Kuala Terengganu">Politeknik Kuala Terengganu</option>
                      <option value="Politeknik Kuching Sarawak">Politeknik Kuching Sarawak</option>
                      <option value="Politeknik Muadzam Shah">Politeknik Muadzam Shah</option>
                      <option value="Politeknik Seberang Perai">Politeknik Seberang Perai</option>
                      <option value="Politeknik Sultan Abdul Halim Mu'adzam Shah">Politeknik Sultan Abdul Halim Mu'adzam Shah</option>
                      <option value="Politeknik Sultan Haji Ahmad Shah">Politeknik Sultan Haji Ahmad Shah</option>
                      <option value="Politeknik Sultan Idris Shah">Politeknik Sultan Idris Shah</option>
                      <option value="Politeknik Sultan Mizan Zainal Abidin">Politeknik Sultan Mizan Zainal Abidin</option>
                      <option value="Politeknik Ungku Omar">Politeknik Ungku Omar</option>
                      <option value="Universiti Kebangsaan Malaysia (UKM)">Universiti Kebangsaan Malaysia (UKM)</option>
                      <option value="Universiti Malaya (UM)">Universiti Malaya (UM)</option>
                      <option value="Universiti Malaysia Kelantan (UMK)">Universiti Malaysia Kelantan (UMK)</option>
                      <option value="Universiti Malaysia Pahang (UMP)">Universiti Malaysia Pahang (UMP)</option>
                      <option value="Universiti Malaysia Perlis(UNIMAP)">Universiti Malaysia Perlis(UNIMAP)</option>
                      <option value="Universiti Malaysia Sabah (UMS)">Universiti Malaysia Sabah (UMS)</option>
                      <option value="Universiti Malaysia Sarawak (UNIMAS)">Universiti Malaysia Sarawak (UNIMAS)</option>
                      <option value="Universiti Malaysia Sabah (UMS)">Universiti Malaysia Sabah (UMS)</option>
                      <option value="Universiti Malaysia Terengganu (UMT)">Universiti Malaysia Terengganu (UMT)</option>
                      <option value="Universiti Pendidikan Sultan Idris (UPSI)">Universiti Pendidikan Sultan Idris (UPSI)</option>
                      <option value="Universiti Pertahanan Nasional Malaysia (UPNM)">Universiti Pertahanan Nasional Malaysia (UPNM)</option>
                      <option value="Universiti Putra Malaysia (UPM)"> Universiti Putra Malaysia (UPM)</option>
                      <option value="Universiti Sains Islam Malaysia (USIM)">Universiti Sains Islam Malaysia (USIM)</option>
                      <option value="Universiti Sains Malaysia (USM)">Universiti Sains Malaysia (USM)</option>
                      <option value="Universiti Sultan Zainal Abidin (UniSZA)">Universiti Sultan Zainal Abidin (UniSZA)</option>
                      <option value="Universiti Teknikal Malaysia Melaka">Universiti Teknikal Malaysia Melaka</option>
                      <option value="Universiti Teknologi Malaysia (UTM)">Universiti Teknologi Malaysia (UTM)</option>
                      <option value="Universiti Teknologi Mara (UiTM)">Universiti Teknologi Mara (UiTM)</option>
                      <option value="Universiti Tun Hussein Onn Malaysia (UTHM)">Universiti Tun Hussein Onn Malaysia (UTHM)</option>
                      <option value="Universiti Utara Malaysia (UUM) ">Universiti Utara Malaysia (UUM) </option>  
                    </select>
                    <p id="inst-error-name" class="error"><?php echo $lang['inst-name-required']; ?></p>
                    

                    <input type="email" id="inst-email" class="input email" name="inst-email" placeholder="<?php echo $lang['inst-email']; ?>" required>
                    <p id="inst-error-email" class="error"><?php echo $lang['inst-email-required']; ?></p>
                    <p id="inst-error-email-pattern" class="error"><?php echo $lang['valid-email-pattern']; ?></p>

                    <input type="submit" id="submit-verify-btn" class="form-submit-button verify-email-button" name="submit" value="<?php echo $lang['sendemail']; ?>">
                  </form>
                </div>   
              </div>
            </div> 
        </div>

      </div>
    </section><!-- #verifyaccount -->

    <div id="verify-respond" class="modal">
      <h3><?php echo $lang['welcomeverify']; ?>, Mohamad!</h3>
      <p><?php echo $lang['verifymsg']; ?></p>
      <a href="#" class="button" rel="modal:close">OK</a>
    </div>

  </main>

<?php
include 'footer.php';
?>